//
//  ViewController.swift
//  ScobicVan
//
//  Created by Moses Olawoyin on 11/12/2019.
//  Copyright © 2019 Moses Olawoyin. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    var segueEditingString: String!
    var segueCoreDataObject: NSManagedObject!
    
    //outlets
    @IBOutlet var tableView: UITableView!
    
    //variables
    var itemArray = [Item]()
    
    
    
    //constants
    let cellid = "cellId"
    let coreDataController = CoreDataController.shared

    

    override func viewDidLoad() {
        super.viewDidLoad()
        cellDelegate()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        fetchItems{ (done) in
            if done {
                if itemArray.count > 0 {
                    tableView.isHidden = false
                }else {
                    tableView.isHidden = true
                }
            }
        }
        tableView.reloadData()
    }
    
    func cellDelegate() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.isHidden = true
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellid, for: indexPath) as! TableViewCell
        let item = itemArray[indexPath.row]
        
        cell.itemLabel.text = item.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let destVC : EditViewController = segue.destination as! EditViewController
        print(self.segueEditingString)
        destVC.editString = self.segueEditingString
        destVC.segueCoreDataObject = segueCoreDataObject
    }
    
   //IOS 13 delete function
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let contextItemDelete = UIContextualAction(style: .destructive, title: "delete") {  (contextualAction, view, boolValue) in
            let ejectedItem = self.itemArray.remove(at: indexPath.row)
            self.coreDataController.delete(ejectedItem)
            let indexPaths = [indexPath]
            tableView.deleteRows(at: indexPaths, with: .automatic)
        }
        
        let contextItemEdit = UIContextualAction(style: .destructive, title: "edit") {  (contextualAction, view, boolValue) in
       //methods
           
            print(self.itemArray[indexPath.row].name!)
            self.segueEditingString = self.itemArray[indexPath.row].name!
            self.segueCoreDataObject = self.itemArray[indexPath.row]
            self.performSegue(withIdentifier: "editItemSegue", sender: self)
            
               
//                       var segue: UIStoryboardSegue!
//                       if segue.identifier! == "editItemSegue" {
//
//                        var selectedItem: NSManagedObject = self.itemArray[indexPath.row] as NSManagedObject
//                        let IVC: EditViewController = segue.destination as! EditViewController
//
//                           IVC.notes = selectedItem.valueForKey("notes") as String
//                           IVC.existingItem = selectedItem

//                       }
        }
        let swipeActions = UISwipeActionsConfiguration(actions: [contextItemDelete, contextItemEdit])

        return swipeActions


    }
    
}

extension ViewController {
    
    func fetchItems (completion: (_ complete: Bool)-> ()) {
        let allItems: NSFetchRequest <Item> = Item.fetchRequest()
        do {
            itemArray =  try coreDataController.mainContex.fetch(allItems)
            completion(true)
            
        }catch{
            print(error.localizedDescription)
            completion(false)
        }
    }
    
}
