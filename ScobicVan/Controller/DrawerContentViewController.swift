//
//  DrawerContentViewController.swift
//  ScobicVan
//
//  Created by Moses Olawoyin on 13/12/2019.
//  Copyright © 2019 Moses Olawoyin. All rights reserved.
//

import UIKit
import Pulley

class DrawerContentViewController: UIViewController {
    
    
    @IBOutlet var drawerTableView: UITableView!
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


}

extension DrawerContentViewController: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CellId", for: indexPath) as! TableViewCell
            return cell
        
    }
    
    
}
