//
//  EditViewController.swift
//  ScobicVan
//
//  Created by Moses Olawoyin on 15/12/2019.
//  Copyright © 2019 Moses Olawoyin. All rights reserved.
//

import UIKit
import CoreData

class EditViewController: UIViewController {
    
     let coreDataController: CoreDataController!
    
    @IBOutlet var editItem: UITextField!
    
    var editString: String!
    var segueCoreDataObject: NSManagedObject!
    
    @IBAction func editDone(_ sender: UIButton) {
        segueCoreDataObject.setValue(editItem.text, forKey: "name")
        print(segueCoreDataObject)
         coreDataController.saveContext()
       
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editItem.text = editString
        // Do any additional setup after loading the view.
    }
    

}
