//
//  AddItemViewController.swift
//  ScobicVan
//
//  Created by Moses Olawoyin on 11/12/2019.
//  Copyright © 2019 Moses Olawoyin. All rights reserved.
//

import UIKit
import CoreData

class AddItemViewController: UIViewController {
    
    
    
    //Outlets
    @IBOutlet var addItemTextField: UITextField!
    
    
    //Variables
    
    
    //Constants
    let coreDataController = CoreDataController.shared

    
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func saveButton(_ sender: UIButton) {
        saveItem { (done) in
            if done {
                navigationController?.popViewController(animated: true)
            }else{
                print("Try Again")
            }
        }
    }
    
    func saveItem( completion: (_ finished: Bool) -> ()) {
        let mainContext = coreDataController.mainContex
        let newItem = Item(context: mainContext)
        newItem.name = addItemTextField.text
        let success = coreDataController.saveContext()
        completion(success)
    }

}
