//
//  TableViewCell.swift
//  ScobicVan
//
//  Created by Moses Olawoyin on 11/12/2019.
//  Copyright © 2019 Moses Olawoyin. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    //Outlets
    
    @IBOutlet var itemLabel: UILabel!
    
    //Variables
    
    
    //Constants

    
    
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
